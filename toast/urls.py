from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework.authtoken import views as authtoken_views

from . import views, views_api


api_v1_router = routers.DefaultRouter()
api_v1_router.register('events', views_api.EventViewSet, basename='event')
api_v1_router.register('leagues', views_api.LeagueViewSet)
api_v1_router.register('teams', views_api.TeamViewSet)
api_v1_router.register('roles', views_api.RoleViewSet)
api_v1_router.register('positions', views_api.PositionViewSet)
api_v1_router.register('games', views_api.GameViewSet)
api_v1_router.register('associations', views_api.AssociationViewSet)
api_v1_router.register('my_availabilities', views_api.MyAvailabilitiesViewSet, basename='availability')

api_v1_urls = [
    path('api-token-auth', authtoken_views.obtain_auth_token),
    path('', include(api_v1_router.urls))
]

api_urls = [
    path('v1/', include((api_v1_urls, 'toast'), namespace='v1'))
]

urlpatterns = [
    path('', views.index, name='index'),
    path('calendar.ics', views.ical, name='ical'),
    path('tournament_cal.ics', views.tournament_ical, name='tournament_ical'),
    path('api/', include(api_urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('home/', views.home, name='home'),
    path('create_account/', views.create_account, name='create_account'),
    path('myprofile/', views.myprofile, name='myprofile'),
    path('currentseason/', views.current_season, name='current_season'),
    path('currentseasoncal/', views.current_season_cal, name='current_season_cal'),
    path('edit_availability/<int:eventid>', views.edit_availability, name='edit_availability'),
    path('del_availability/<int:availabilityid>', views.del_availability, name='del_availability'),
    path('availability/<int:availabilityid>/select_official/<int:roleid>', views.select_official, name='select_official'),
    path('availability/<int:availabilityid>/unselect_official', views.unselect_official, name='unselect_official'),
    path('availability/<int:availabilityid>/nomination', views.edit_availability_nomination, name='edit_availability_nomination'),
    path('event/<int:eventid>', views.show_event, name='show_event'),
    path('event/<int:eventid>/delete', views.del_event, name='delete_event'),
    path('event/<int:eventid>/csv', views.export_event_to_csv, name='export_event_to_csv'),
    path('event/<int:event_id>/lock_in_crew_and_email/<int:role_id>', views.lock_in_crew_role_and_email, name='lock_in_crew_role_and_email'),
    path('event/<int:event_id>/lock_in_crew/<int:role_id>', views.lock_in_crew_role, name='lock_in_crew_role'),
    path('event/<int:eventid>/make_crew_head/<int:personid>', views.make_crew_head, name='make_crew_head'),
    path('event/<int:eventid>/remove_crew_head/<int:personid>', views.remove_crew_head, name='remove_crew_head'),
    path('edit_event/<int:eventid>', views.edit_event, name='edit_event'),
    path('your_events/', views.your_events, name='your_events'),
    path('your_availabilities/', views.your_avails, name='your_avails'),
    path('league_rep_report/', views.league_rep_report, name='league_rep_report'),
    path('delete_account/', views.delete_account, name='delete_account'),
    path('really_delete_account/', views.really_delete_account, name='really_delete_account'),
    path('about/', TemplateView.as_view(template_name='toast/about.html'), name='about'),
    path('create_event/', views.create_event, name='create_event'),
    path('create_league/', views.create_league, name='create_league'),
    path('leagues/<int:leagueid>', views.edit_league, name='league'),
    path('leagues/<int:league_id>/reps/<int:person_id>/delete', views.sack_rep, name='sack_rep'),
    path('leagues/<int:league_id>/teams/<int:team_id>/delete', views.delete_team, name='delete_team'),
    path('housekeeping/springclean', views.admin_spring_clean, name='spring_clean'),
    path('event/<int:event_id>/notifyofficials', views.send_message_from_heads_to_officials_view, name='send_message_from_heads_to_officials'),
    path('event/<int:event_id>/notifyheads', views.send_message_from_official_to_heads_view, name='send_message_from_official_to_heads'),
    path('stats/', views.application_stats, name='application_stats')
]
