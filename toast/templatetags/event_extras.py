from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.simple_tag(takes_context=True)
def days_remain_style(context, days_remain):
    # above 30 - green
    # below 30 - fade to red through yellow
    # so 90 degrees of hue
    green_hue = 90
    day_horizon = 30
    if days_remain < day_horizon:
        green_hue = days_remain/day_horizon * green_hue

    return f'hsl({green_hue}, 100%, 66%)'


@register.filter(name='render_contact_method', is_safe=True)
def render_contact_method(contact_method):
    # Render small text in all cases.
    # If link - make A-tag
    # if *almost* a facebook url - make A-tag
    # Be nice to add a copy-to-clipboard icon thing
    # TODO: copy-to-clipboard icons? FB link icons? Make visually appealing again
    is_url = False
    if contact_method.startswith('/'):
        is_url = True
        contact_method = f'https://facebook.com{contact_method}'
    elif contact_method.lower().startswith('facebook.com'):
        is_url = True
        contact_method = f'https://{contact_method}'
    elif contact_method.lower().startswith('www'):
        is_url = True
        contact_method = f'https://{contact_method}'
    elif contact_method.lower().startswith('http'):
        is_url = True

    text = f'<small>{contact_method} <a href="mailto:{contact_method}" class="text-decoration-none">📧</a></small>'
    if is_url:
        text = f'<a href={contact_method} target="_blank" class="btn btn-outline-secondary btn-sm">Facebook</a>'
    return mark_safe(text)
